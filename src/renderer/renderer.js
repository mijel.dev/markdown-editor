
document.addEventListener("DOMContentLoaded", () => {

  const content = document.getElementById("content");

  document.getElementById("config-link").addEventListener("click", (event) => {
    event.preventDefault();
    loadPage("config");
  });

  document.getElementById("about-link").addEventListener("click", (event) => {
    event.preventDefault();
    loadPage("about");
  });

  // loadPage("config");

  function loadPage(page) {
    const pagePath = `./pages/${page}/index.html`;
    fetch(pagePath)
      .then((response) => response.text())
      .then((html) => {
        content.innerHTML = html;
        initializeEditor();  // Inicializa el editor después de cargar el contenido
      })
      .catch((error) => {
        console.error("Error loading page:", error);
        content.innerHTML = "<p>Error loading page.</p>";
      });
  }

  function initializeEditor() {
    const textarea = document.getElementById("editor");
    if (textarea) {
      new SimpleMDE({ element: textarea });
    }
  }
  
  // Initialize editor on initial load if needed
  if (document.getElementById("editor")) {
    initializeEditor();
  }
});
